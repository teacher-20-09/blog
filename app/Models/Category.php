<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
  protected $fillable = ['title', 'slug', 'parent_id', 'published', 'created_by', 'modified_by'];
  /**
   * @var mixed
   */
  private $title;

  public function setSlugAttribute($value)
  {
    $this->attributes['slug'] = Str::slug(mb_substr($this->title, 0, 40) . '-' .\Carbon\Carbon::now()->format('dmyHi'), '-');
  }

  public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
  {
    return $this->hasMany(self::class, 'parent_id');
  }

  public function articles(): \Illuminate\Database\Eloquent\Relations\MorphToMany
  {
    return $this->morphedByMany('App\Models\Article', 'categoryable');
  }

  public function scopeLastCategories($query, $count)
  {
    return $query->orderBy('created_at', 'desc')->take($count)->get();
  }
}
