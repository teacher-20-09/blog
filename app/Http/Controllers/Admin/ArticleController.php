<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ArticleController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Application|Factory|View|Response
   */
  public function index()
  {
    return view('admin.articles.index', [
        'articles' => Article::orderBy('created_at', 'desc')->paginate(10)
    ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Application|Factory|View|Response
   */
  public function create()
  {
    return view('admin.articles.create', [
        'article' => [],
        'categories' => Category::with('children')->where('parent_id', 0)->get(),
        'delimiter' => ''
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return RedirectResponse
   */
  public function store(Request $request): RedirectResponse
  {
    $article = Article::create($request->all());

    if ($request->input('categories')) :
      $article->categories()->attach($request->input('categories'));
    endif;

    return redirect()->route('admin.article.index');
  }

  /**
   * Display the specified resource.
   *
   * @param Article $article
   * @return Response
   */
  public function show(Article $article)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param Article $article
   * @return Application|Factory|View|Response
   */
  public function edit(Article $article)
  {
    return view('admin.articles.edit', [
        'article' => $article,
        'categories' => Category::with('children')->where('parent_id', 0)->get(),
        'delimiter' => ''
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Article $article
   * @return RedirectResponse
   */
  public function update(Request $request, Article $article)
  {
    $article->update($request->except('slug'));

    $article->categories()->detach();
    if ($request->input('categories')) :
      $article->categories()->attach($request->input('categories'));
    endif;

    return redirect()->route('admin.article.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Article $article
   * @return RedirectResponse
   * @throws Exception
   */
  public function destroy(Article $article)
  {
    $article->categories()->detach();
    $article->delete();

    return redirect()->route('admin.article.index');
  }
}
