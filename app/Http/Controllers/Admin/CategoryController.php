<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Application|Factory|View|Response
   */
  public function index()
  {
    return view('admin.categories.index', [
        'categories' => Category::paginate(8)
      ]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Application|Factory|View|Response
   */
  public function create()
  {
    return view('admin.categories.create', [
        'category' => [],
        'categories' => Category::with('children')->where('parent_id', '0')->get(),
        'delimiter' => ''
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return RedirectResponse
   */
  public function store(Request $request): RedirectResponse
  {
    Category::create($request->all());

    return redirect()->route('admin.category.index');
  }

  /**
   * Display the specified resource.
   *
   * @param Category $category
   * @return Response
   */
  public function show(Category $category): Response
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param Category $category
   * @return Application|Factory|View|Response
   */
  public function edit(Category $category)
  {
    return view('admin.categories.edit', [
        'category' => $category,
        'categories' => Category::with('children')->where('parent_id', '0')->get(),
        'delimiter' => ''
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param Category $category
   * @return RedirectResponse
   */
  public function update(Request $request, Category $category): RedirectResponse
  {
    $category->update($request->except('slug'));

    return redirect()->route('admin.category.index');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param Category $category
   * @return RedirectResponse
   * @throws Exception
   */
  public function destroy(Category $category): RedirectResponse
  {
    $category->delete();

    return redirect()->route('admin.category.index');
  }
}
