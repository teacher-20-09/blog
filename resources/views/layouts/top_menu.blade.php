@foreach($categories as $category)
  @if($category->children()->where('published')->count())

    <div class="dropdown">
      <a class="btn btn-secondary dropdown-toggle" href="{{url("/blog/category/$category->slug")}}"
         role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-expanded="false">
        {{ $category->title }}
      </a>

      <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink" role="menu">
        @include('layouts.top_menu', ['categories' => $category->children])
      </ul>
    </div>
    @else
      <ul class="nav navbar-nav">
        <li class="nav-item"><a class="nav-link" href="{{url("/blog/category/$category->slug")}}">{{ $category->title }}</a></li>
      </ul>

  @endif

@endforeach