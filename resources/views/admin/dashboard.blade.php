@extends('admin.layouts.app_admin')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <div class="jumbotron">
          <button type="button" class="btn btn-primary">
            Categories <span class="badge badge-light">{{ $count_categories }}</span>
          </button>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="jumbotron">
          <button type="button" class="btn btn-primary">
            Materials <span class="badge badge-light">{{ $count_articles }}</span>
          </button>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="jumbotron">
          <button type="button" class="btn btn-primary">
            Visitors <span class="badge badge-light">0</span>
          </button>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="jumbotron">
          <button type="button" class="btn btn-primary">
            Today <span class="badge badge-light">0</span>
          </button>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-6">
        <a href="{{route('admin.category.create')}}" class="btn btn-block btn-light">Create category</a>

        @foreach($categories as $category)
          <div class="card text-dark bg-light mb-3">
            <a class="category__item" href="{{ route('admin.category.edit', $category) }}">
              <div class="card-header">
                <h5 class="list-group-item-heading m-0">{{ $category->title }}</h5>
                <h5><span class="badge category__item-badge bg-secondary">{{ $category->articles()->count() }}</span></h5>
              </div>
            </a>
          </div>
        @endforeach
      </div>

      <div class="col-6">
        <a href="{{route('admin.article.create')}}" class="btn btn-block btn-light">Create material</a>

        @foreach($articles as $article)
          <div class="card text-dark bg-light mb-3">
            <a class="article__item" href="{{ route('admin.article.edit', $article) }}">
              <div class="card-header">
                <h5 class="list-group-item-heading m-0">{{ $article->title }}</h5>
              </div>
              <div class="card-body">
                <p class="list-group-item-text m-0">{{ $article->pluck('title')->implode(', ') }}</p>
              </div>
            </a>
          </div>
        @endforeach
      </div>
    </div>
  </div>
@endsection