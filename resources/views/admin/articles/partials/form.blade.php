<label for="status">Status</label>

<select class="form-control" name="published" id="status">
  @if(isset($article->id))
    <option value="0" @if($article->published == 0) selected="" @endif>Not published</option>
    <option value="1" @if($article->published == 1) selected="" @endif>Published</option>
  @else
    <option value="0">Not published</option>
    <option value="1">Published</option>
  @endif
</select>

<label class="mt-4" for="title">Title</label>
<input class="form-control" id="title" type="text" name="title" value="{{ $article->title ?? "" }}" placeholder="Article title"
       required>

<label class="mt-4" for="slug">Slug (Uniq value)</label>
<input class="form-control" id="slug" type="text" name="slug" value="{{ $article->slug ?? "" }}"
       placeholder="Automatic generation" readonly="">

<label class="mt-4" for="parent">Parent category</label>
<select class="form-control" id="parent" name="categories[]" multiple="">
    @include('admin.articles.partials.categories', ['categories'=> $categories])
</select>

<label class="mt-4" for="description_short">Short description</label>
<textarea class="form-control" name="description_short" id="description_short">{{ $article->description_short ?? "" }}</textarea>

<label class="mt-4" for="description">Description</label>
<textarea class="form-control" name="description" id="description">{{ $article->description ?? "" }}</textarea>

<hr>

<label class="mt-4" for="meta_title">Meta title</label>
<input class="form-control" id="meta_title" type="text" name="meta_title" value="{{ $article->meta_title ?? "" }}"
       placeholder="Meta title">

<label class="mt-4" for="meta_desc">Meta description</label>
<input class="form-control" id="meta_desc" type="text" name="meta_description" value="{{ $article->meta_description ?? "" }}"
       placeholder="Meta description">

<label class="mt-4" for="meta_keyword">Keywords</label>
<input class="form-control" id="meta_keyword" type="text" name="meta_keyword" value="{{ $article->meta_keyword ?? "" }}"
       placeholder="Keywords">

<hr>

<input class="btn btn-primary" type="submit" value="Save">

<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

<script>
  tinymce.init({
    selector: "textarea#description",
    skin: "bootstrap",
    plugins: "lists, link, image, media",
    toolbar: "h1 h2 bold italic strikethrough blockquote bullist numlist backcolor | link image media | removeformat help",
    menubar: false,
  });
  tinymce.init({
    selector: "textarea#description_short",
    skin: "bootstrap",
    plugins: "lists, link, image, media",
    toolbar: "h1 h2 bold italic strikethrough blockquote bullist numlist backcolor | link image media | removeformat help",
    menubar: false,
  });
</script>