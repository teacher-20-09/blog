@extends('admin.layouts.app_admin')

@section('content')
  <div class="container">
    @component('admin.components.breadcrumb')
      @slot('title') Articles list @endslot
      @slot('parent') Main @endslot
      @slot('active') Articles @endslot
    @endcomponent

    <hr>

    <a href="{{route('admin.article.create')}}" class="btn btn-primary float-right mb-3">
      <b>+</b> Create article
    </a>

    <table class="table">
      <thead>
      <tr>
        <th>Name</th>
        <th>Published</th>
        <th class="text-right">Action</th>
      </tr>
      </thead>
      <tbody>
      @forelse($articles as $article)
        <tr>
          <th>{{ $article->title }}</th>
          <td>{{ $article->published }}</td>
          <td class="text-right">
            <form onsubmit="return confirm('Delete article?');"
                  action="{{ route('admin.article.destroy', $article) }}"
                  method="post">
              <input type="hidden" name="_method" value="DELETE">
              {{ csrf_field() }}

              <a class="btn btn-default p-0" href="{{ route('admin.article.edit',$article) }}">🖋️</a>
              <button class="btn p-0" type="submit">🗑</button>
            </form>
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="3" class="text-center"><h4>No data available</h4></td>
        </tr>
      @endforelse
      </tbody>
    </table>

    <div class="paginate text-center">
      {{ $articles->links() }}
    </div>
  </div>
@endsection