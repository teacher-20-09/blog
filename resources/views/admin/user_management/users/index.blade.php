@extends('admin.layouts.app_admin')

@section('content')
  <div class="container">
    @component('admin.components.breadcrumb')
      @slot('title') Users list @endslot
      @slot('parent') Main @endslot
      @slot('active') Users @endslot
    @endcomponent

    <hr>

    <a href="{{route('admin.user_management.user.create')}}" class="btn btn-primary float-right mb-3">
      <b>+</b> Create user
    </a>

    <table class="table">
      <thead>
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th class="text-right">Action</th>
      </tr>
      </thead>
      <tbody>
      @forelse($users as $user)
        <tr>
          <th>{{ $user->name }}</th>
          <td>{{ $user->email }}</td>
          <td class="text-right">
            <form onsubmit="return confirm('Delete article?');" action="{{ route('admin.user_management.user.destroy', $user) }}"
                  method="post">
              {{ method_field('DELETE') }}
              {{ csrf_field() }}

              <a class="btn btn-default p-0" href="{{ route('admin.user_management.user.edit',$user) }}">🖋️</a>
              <button class="btn p-0" type="submit">🗑</button>
            </form>
          </td>
        </tr>
      @empty
        <tr>
          <td colspan="3" class="text-center"><h4>No data available</h4></td>
        </tr>
      @endforelse
      </tbody>
    </table>

    <div class="paginate text-center">
      {{ $users->links() }}
    </div>
  </div>
@endsection