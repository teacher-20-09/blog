@if($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif

<label class="mt-4" for="name">Name</label>
<input class="form-control" id="name" type="text" name="name"
       value="@if(old('name')) {{ old('name') }}@else{{ $user->name ?? "" }} @endif" placeholder="Name"
       required>

<label class="mt-4" for="email">Email</label>
<input class="form-control" id="email" type="text" name="email"
       value="@if(old('email')) {{ old('email') }}@else{{ $user->email ?? "" }} @endif" placeholder="Email"
       required>

<label class="mt-4" for="password">Password</label>
<input class="form-control" id="password" type="password" name="password" value="{{ $user->password ?? "" }}"
       placeholder="Password"
       required>

<label class="mt-4" for="password_confirmation">Confirm password</label>
<input class="form-control" id="password_confirmation" type="password" name="password_confirmation"
       value="{{ $user->confirm ?? "" }}" placeholder="Confirm password"
       required>

<hr>

<input class="btn btn-primary" type="submit" value="Save">