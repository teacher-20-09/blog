@extends('layouts.app')

@section('title', $article->meta_title)
@section('meta_keyword', $article->meta_keyword)
@section('meta_description', $article->meta_description)

@section('content')
  <div class="container">
    @forelse($articles as $article)
      <div class="row">
        <div class="col-12">
          <h4>{{ $article->title }}</h4>
          <p>{!! $article->description !!}</p>
        </div>
      </div>
    @empty
      <h4 class="text-center">Empty</h4>
    @endforelse
  </div>

  <div class="paginate text-center">
    {{ $articles->links() }}
  </div>
@endsection