@extends('layouts.app')

@section('title', $category->title . " -blog-")

@section('content')
  <div class="container">
    @forelse($articles as $article)
      <div class="row">
        <div class="col-12">
          <h4><a href="{{route('article', $article->slug)}}">{{ $article->title }}</a></h4>
          <p>{!! $article->description_short !!}</p>
        </div>
      </div>
    @empty
      <h4 class="text-center">Empty</h4>
    @endforelse
  </div>

  <div class="paginate text-center">
    {{ $articles->links() }}
  </div>
@endsection